@if "%DEBUG%" == "" @echo off
@rem ##########################################################################
@rem
@rem  ApplicationLogic startup script for Windows
@rem
@rem ##########################################################################

@rem Set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" setlocal

set DIRNAME=%~dp0
if "%DIRNAME%" == "" set DIRNAME=.
set APP_BASE_NAME=%~n0
set APP_HOME=%DIRNAME%..

@rem Add default JVM options here. You can also use JAVA_OPTS and APPLICATION_LOGIC_OPTS to pass JVM options to this script.
set DEFAULT_JVM_OPTS=

@rem Find java.exe
if defined JAVA_HOME goto findJavaFromJavaHome

set JAVA_EXE=java.exe
%JAVA_EXE% -version >NUL 2>&1
if "%ERRORLEVEL%" == "0" goto init

echo.
echo ERROR: JAVA_HOME is not set and no 'java' command could be found in your PATH.
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:findJavaFromJavaHome
set JAVA_HOME=%JAVA_HOME:"=%
set JAVA_EXE=%JAVA_HOME%/bin/java.exe

if exist "%JAVA_EXE%" goto init

echo.
echo ERROR: JAVA_HOME is set to an invalid directory: %JAVA_HOME%
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:init
@rem Get command-line arguments, handling Windows variants

if not "%OS%" == "Windows_NT" goto win9xME_args

:win9xME_args
@rem Slurp the command line arguments.
set CMD_LINE_ARGS=
set _SKIP=2

:win9xME_args_slurp
if "x%~1" == "x" goto execute

set CMD_LINE_ARGS=%*

:execute
@rem Setup the command line

set CLASSPATH=%APP_HOME%\lib\ApplicationLogic.jar;%APP_HOME%\lib\hibernate-tools-5.3.0.CR2.jar;%APP_HOME%\lib\slf4j-log4j12-1.7.23.jar;%APP_HOME%\lib\slf4j-api-1.7.23.jar;%APP_HOME%\lib\hibernate-core-5.3.0.CR2.jar;%APP_HOME%\lib\hibernate-commons-annotations-5.0.3.Final.jar;%APP_HOME%\lib\jboss-logging-3.3.2.Final.jar;%APP_HOME%\lib\javax.persistence-api-2.2.jar;%APP_HOME%\lib\javassist-3.22.0-GA.jar;%APP_HOME%\lib\byte-buddy-1.8.0.jar;%APP_HOME%\lib\antlr-2.7.7.jar;%APP_HOME%\lib\jboss-transaction-api_1.2_spec-1.0.1.Final.jar;%APP_HOME%\lib\jandex-2.0.3.Final.jar;%APP_HOME%\lib\classmate-1.3.0.jar;%APP_HOME%\lib\dom4j-1.6.1.jar;%APP_HOME%\lib\commons-collections-3.2.2.jar;%APP_HOME%\lib\commons-logging-1.2.jar;%APP_HOME%\lib\jaxen-1.1.6.jar;%APP_HOME%\lib\ant-1.10.1.jar;%APP_HOME%\lib\org.eclipse.jdt.core-3.12.2.jar;%APP_HOME%\lib\freemarker-2.3.23.jar;%APP_HOME%\lib\hibernate-jpa-2.1-api-1.0.0.Final.jar;%APP_HOME%\lib\ant-launcher-1.10.1.jar;%APP_HOME%\lib\org.eclipse.core.resources-3.12.0.jar;%APP_HOME%\lib\org.eclipse.core.expressions-3.6.0.jar;%APP_HOME%\lib\org.eclipse.core.runtime-3.13.0.jar;%APP_HOME%\lib\org.eclipse.core.filesystem-1.7.0.jar;%APP_HOME%\lib\org.eclipse.text-3.6.100.jar;%APP_HOME%\lib\log4j-1.2.17.jar;%APP_HOME%\lib\org.eclipse.osgi-3.12.100.jar;%APP_HOME%\lib\org.eclipse.core.jobs-3.9.3.jar;%APP_HOME%\lib\org.eclipse.core.contenttype-3.6.0.jar;%APP_HOME%\lib\org.eclipse.equinox.app-1.3.400.jar;%APP_HOME%\lib\org.eclipse.equinox.registry-3.7.0.jar;%APP_HOME%\lib\org.eclipse.equinox.preferences-3.7.0.jar;%APP_HOME%\lib\org.eclipse.core.commands-3.9.0.jar;%APP_HOME%\lib\org.eclipse.equinox.common-3.9.0.jar

@rem Execute ApplicationLogic
"%JAVA_EXE%" %DEFAULT_JVM_OPTS% %JAVA_OPTS% %APPLICATION_LOGIC_OPTS%  -classpath "%CLASSPATH%" de.gradleHiber.App %CMD_LINE_ARGS%

:end
@rem End local scope for the variables with windows NT shell
if "%ERRORLEVEL%"=="0" goto mainEnd

:fail
rem Set variable APPLICATION_LOGIC_EXIT_CONSOLE if you need the _script_ return code instead of
rem the _cmd.exe /c_ return code!
if  not "" == "%APPLICATION_LOGIC_EXIT_CONSOLE%" exit 1
exit /b 1

:mainEnd
if "%OS%"=="Windows_NT" endlocal

:omega
